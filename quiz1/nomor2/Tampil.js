import React, {useState, useContext} from 'react';
import {View, Text, FlatList, StyleSheet} from 'react-native';

import {RootContext} from './Soal2';

const Tampil = () => {
  const state = useContext(RootContext);

  const renderItem = ({item, index}) => {
    return (
      <View style={styles.taskWrapper}>
        <View style={styles.lisTask}>
          <Text style={styles.name}>{item.name}</Text>
          <Text style={styles.position}>{item.position}</Text>
        </View>
      </View>
    );
  };

  return (
    <View>
      <Text style={styles.header}>DAFTAR TRAINER</Text>
      <FlatList data={state.name} renderItem={renderItem} />
    </View>
  );
};
export default Tampil;

const styles = StyleSheet.create({
  header: {
    justifyContent: 'center',
    textAlign: 'center',
    fontWeight: 'bold',
    padding: 15,
    fontSize: 18,
  },
  taskWrapper: {
    marginTop: '5%',
    borderColor: '#c9c9c9',
    borderWidth: 3,
    minHeight: 30,
    borderRadius: 6,
    marginHorizontal: 10,
  },
  lisTask: {
    width: '90%',
  },
  name: {
    padding: 10,
    color: '#1c1c1c',
    fontWeight: 'bold',
  },
  position: {
    padding: 10,
    color: '#1c1c1c',
  },
});
