import React, {useState, createContext} from 'react';
import Tampil from './Tampil';

export const RootContext = createContext();

const Soal2 = () => {
  const [name, setName] = useState([
    {
      name: 'Zakky Muhammad Fajar',
      position: 'Trainer 1 React Native Lanjutan',
    },
    {
      name: 'Mukhlis Hanafi',
      position: 'Trainer 2 React Native Lanjutan',
    },
  ]);

  return (
    <RootContext.Provider
      value={{
        name,
      }}>
      <Tampil />
    </RootContext.Provider>
  );
};

export default Soal2;
