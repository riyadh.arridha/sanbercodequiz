import React, {useEffect, useState} from 'react';
import {Text, View} from 'react-native';

const Soal1 = () => {
  const [name, setName] = useState('');

  useEffect(() => {
    setTimeout(() => {
      setName('Asep');
    }, 3000);
  });

  return (
    <View>
      <Text>{name}</Text>
    </View>
  );
};

export default Soal1;
